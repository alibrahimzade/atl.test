package az.atl.academy.controller;

import az.atl.academy.model.dto.StudentDto;
import az.atl.academy.service.StudentService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/student")
@AllArgsConstructor
public class StudentController {
    private final StudentService studentService;

    @GetMapping("/")
    public String getAllStudents(Model model) {
        model.addAttribute("allStudentlist", studentService.getAll());
        return "index";
    }

    @GetMapping("/addnew")
    public String addNewStudent(Model model) {
        StudentDto student = new StudentDto();
        model.addAttribute("student", student);
        return "newstudent";
    }

    @PostMapping("/save")
    public String saveStudent(@ModelAttribute("student") StudentDto studentDto) {
        studentService.create(studentDto);
        return "redirect:/student/";
    }

    @GetMapping("/showFormForUpdate/{id}")
    public String updateForm(@PathVariable(value = "id") long id, Model model) {
        StudentDto student = studentService.getById(id);
        model.addAttribute("student", student);
        return "update";
    }

    @GetMapping("/deleteStudent/{id}")
    public String deleteThroughId(@PathVariable(value = "id") long id) {
        studentService.deleteById(id);
        return "redirect:/student/";

    }
}
